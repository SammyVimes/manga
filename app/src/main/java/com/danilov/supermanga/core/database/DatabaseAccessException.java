package com.danilov.supermanga.core.database;

/**
 * Created by Semyon Danilov on 04.07.2014.
 */
public class DatabaseAccessException extends Exception {

    public DatabaseAccessException(final String message) {
        super(message);
    }

}
